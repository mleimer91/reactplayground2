import React from 'react';

import Coordinates from './Coordinates';

/**
 * this is a stateless functional react component. (" a dumb component ")
 * it is a simple js function (or in this case a arrow function) that returns a dom element
 *
 * Note: for the same given props, this will return always the same markup.
 */
const SbbStationList = ({stations}) => {
  if (!stations || !stations.length) {
    return (
      <ul className='location-list list-group'>
        <li className="list-group-item list-group-item-warning">No Results</li>
      </ul>
    );
  }

  return (
    <ul className='location-list list-group'>
      {stations.map((station, index) =>
        // the key prop is used to uniquely identify each of the rendered subcomponents
        // like this react can reuse previously rendered components if they are e.g. shuffled or filtered
        <SbbStation key={index} station={station}/>
      )}
    </ul>
  );
};

SbbStationList.propTypes = {
  stations: React.PropTypes.array.isRequired
};


/**
 * This is a stateless functional react component similar to the one above.
 * it does not even contain any logic
 */
const SbbStation = ({station}) => (
  <li className="list-group-item">
    <h4 className="list-group-item-heading">{station.name}</h4>
    <span className="badge">{station.score}</span>
    <p className="list-group-item-text">
      <Coordinates coordinates={station.coordinate}/>
    </p>
  </li>
);

SbbStation.propTypes = {
  station: React.PropTypes.object.isRequired
};


export default SbbStationList;
