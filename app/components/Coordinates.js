// we always need to import react, since the jsx code in this file is transpiled to something like "React.component('span',...)"
import React from 'react';

const Coordinates = ({coordinates}) => {

  if (!coordinates) {
    // stateless functional components cannot return null, instead we render an empty span
    return <span></span>;
  }

  if (!coordinates.type || coordinates.type !== 'WGS84') {
    return <span>Unknown coordinates type!</span>
  }

  return <span>{coordinates.x} lat / {coordinates.y} lng</span>;
};

Coordinates.propTypes = {
  coordinates: React.PropTypes.object.isRequired
};


export default Coordinates;
