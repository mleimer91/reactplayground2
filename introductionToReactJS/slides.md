
layout:true

.footer[Introduction to React | CTD 2016]

---

name: CTD2016
class: full

![Client Technology Days](CTD_SlideDeckFolie_4_3.png)

---


name: Title
class: center, middle, title

# An Introduction to React

![React](reactjs.png)

---

name: Agenda

# Agenda

1. The Framework
2. Concepts
3. JSX
4. Component Structure, State & Props
5. ECMAScript2015
6. Exercises
7. Outlook

<hr>

_You can look at this presentation on your own computer using:_
```
npm run serveIntro
```

---
name: Framework

# The Framework

### Developed by facebook & Instagram

### Used by Netflix, Imgur, Airbnb, Feedly, atlassian, dorma+kaba

### As of July 2016, React and React Native are Facebook's top two open-source projects by number of stars on GitHub, and React is the 6th most starred project of all time on GitHub.

---
```
const TodoList = React.createClass({
  render: function() {
    const createItem = (item) => <li key={item.id}>{item.text}</li>;
    return <ul>{this.props.items.map(createItem)}</ul>;
  }
});
const TodoApp = React.createClass({
  getInitialState: function() {
    return {items: [], text: ''};
  },
  onChange: function(e) {
    this.setState({text: e.target.value});
  },
  handleSubmit: function(e) {
    const newItem = [{text: this.state.text, id: Date.now()}];
    const nextItems = this.state.items.concat(newItem);
    this.setState({items: nextItems, text: ''});
  },
  render: function() {
    return (
      <div>
        <h3>TODO</h3>
        <TodoList items={this.state.items} />
        <form onSubmit={this.handleSubmit}>
          <input onChange={this.onChange} value={this.state.text} />
          <button>{'Add #' + (this.state.items.length + 1)}</button>
        </form>
      </div>
    );
  }
});
ReactDOM.render(<TodoApp />, mountNode);
```
---

name: Concepts

# Concepts : 1. Just the UI

*«A JavaScript Library for building User Interfaces»*

* No full-fledged JavaScript MV* Framework
* Additional Libraries for publish/subscribe, data-storage
* The **V** in **MVC**

.width-500[![ui](firefox-everywhere.png)]

---

# Concepts :  2. Virtual DOM

* DOM abstracted
* Performance
* Server-Side Rendering

![virtual DOM](virtualdom.png)

---

# Concepts :  3. Data Flow

* One-way reactive data flow

![One-way data flow](data-flow.png)

---

name: JSX

# JSX

A XML-like syntax extension to ECMAScript

```javascript
const myComponent = <div className="super-div">This is a div</div>;
```

Will be transpiled to

```javascript
const myComponent = React.createElement(
  "div",
  { className: "super-div" },
  "This is a div"
);
```

### Experiment

Visit https://babeljs.io/repl/ in order to try out JSX

---

name: JSX

# JSX (2)

Attribute Expressions

```javascript
const isActive = true;
const myComponent = <div className={isActive ? 'active' : 'inactive'}>This is a div</div>;
```

Child Expressions

```javascript
const isActive = true;
const myComponent = (
  <div>
  {isActive && 
  <h1>Only shown when active</h1>
  }
  </div>
);
```

---

# Component Structure

```javascript
const MyView = React.createClass({

  getDefaultProps() {
    return {
      myClass: 'my-view'
    };
  },
  getInitialState() {
    return {};
  },
  
  onButtonClick() {
    this.setState({
      someAttribute: 'clicked'
    });
  },
  
  render() {
    return (
      <div className={this.props.myClass}>
        <button onClick={this.onButtonClick}>Click me</button>
        {this.state.someAttribute}
      </div>
    );
  }
}
```

```html
<MyView myClass='other-view' />
```

---
name:ES2015_arrowFunctions

# ECMAScript2015 : Arrow Functions

```javascript
[1, 2, 3].map(num => num * 2)
// <- [2, 4, 6]
```

```javascript
[1, 2, 3].map(function (num) { return num * 2 })
// <- [2, 4, 6]
```

If we need to declare multiple (or zero) arguments, we’ll have to use parenthesis.
```javascript
[1, 2, 3, 4].map((num, index) => num * 2 + index)
// <- [2, 5, 8, 11]
```

You might want to have some other statements and not just an expression to return. In this case you’ll have to use braces notation.
```javascript
[1, 2, 3, 4].map(num => {
  var multiplier = 2 + num
  return num * multiplier
})
// <- [3, 8, 15, 24]
```

Source: ES6 Arrow Functions in Depth: https://ponyfoo.com/articles/es6-arrow-functions-in-depth

---
name:ES2015_destructuring

# ECMAScript2015 : Destructuring

```javascript
var foo = { bar: 'pony', baz: 3 }
var {bar, baz} = foo
console.log(bar)
// <- 'pony'
console.log(baz)
// <- 3
```

With aliases
```
var foo = { bar: 'pony', baz: 3 }
var {bar: a, baz: b} = foo
console.log(a)
// <- 'pony'
console.log(b)
// <- 3
```

Nested
```javascript
var foo = { bar: { deep: 'pony', dangerouslySetInnerHTML: 'lol' } }
var {bar: { deep, dangerouslySetInnerHTML: sure }} = foo
console.log(deep)
// <- 'pony'
console.log(sure)
// <- 'lol'
```


---
name:ES2015_destructuring2

# ECMAScript2015 : Destructuring (2)


```javascript
const myArrowFunction = ({a,b}) => {
  console.log(a);
  // <- 'A'
  console.log(b);
  // <- 'B'
};

myArrowFunction({a:'A',b:'B',c:'C'});
```

In ES5
```javascript
var myArrowFunction = function myArrowFunction(_ref) {
  var a = _ref.a;
  var b = _ref.b;

  console.log(a);
  console.log(b);
};
myArrowFunction({ a: 'A', b: 'B', c: 'C' });
```

Source: ES6 JavaScript Destructuring in Depth: https://ponyfoo.com/articles/es6-destructuring-in-depth

---

name:ES2015_var_let_const

# ECMAScript2015 : var let const

`var` is **function-scoped**. Gets pulled to the top of it's scope ("hoisting").
.row[
.six.columns[
```javascript
function areTheyAwesome (name) {
  if (name === 'nico') {
    var awesome = true
  }
  return awesome
}
```
]
.six.columns[
```javascript
function areTheyAwesome (name) {
  var awesome
  if (name === 'nico') {
    awesome = true
  }
  return awesome
}
```
]
]

`let` **is block-scoped**.
.row[
.six.columns[
```javascript
function areTheyAwesomeLet (name) {
  if (name === 'nico') {
    let awesome = true
  }
  return awesome
}
```
]

.six.columns[
```javascript
function areTheyAwesomeLet (name) {
  if (name === 'nico') {
    var _awesome = true
  }
  return awesome
}
```
]

]

---
name:ES2015_var_let_const2

# ECMAScript2015 : var let const (2)

* `const` is also block-scoped
* `const` variables must be declared using an initializer
* `const` variables can only be assigned to once, in said initializer
* `const` variables don’t make the assigned value immutable

```javascript
const cool = { people: ['you', 'me', 'tesla', 'musk'] }
cool = {}
// <- "cool" is read-only
```

```javascript
const cool = { people: ['you', 'me', 'tesla', 'musk'] }
cool.people.push('berners-lee')
console.log(cool)
// <- { people: ['you', 'me', 'tesla', 'musk', 'berners-lee'] }
```

Source: ES6 Let, Const and the “Temporal Dead Zone” (TDZ) in Depth: https://ponyfoo.com/articles/es6-let-const-and-temporal-dead-zone-in-depth



---

# Demo Project Walkthrough

1. index.html
1. app.js: Entry point of application
1. Views
1. Reusable Components



### Before coding
Checkout Example Project
```bash
git clone https://bitbucket.org/ZuehlkeGlobal/reactplayground.git
```
Run
```bash
git pull
npm install
npm run serve
```

---

# Exercises

1. Add a search input to specify a dynamic station instead of "Stadelhofen"
2. Extract the search input to an own component
3. Query and display the next departures when a user clicks a station
   1. API: http://transport.opendata.ch/docs.html#stationboard
   2. Implement the click functionality on SbbStationList
   3. Implement a SbbDepartures component to display the departures
   
### Further Exercises
* Implement your own ideas
* For a example with google maps and [Immutable.js](https://facebook.github.io/immutable-js/) 
  look at: `git checkout extendedExample`

---

# Exercise 1

_Add a search input to specify a dynamic station instead of "Stadelhofen"._

### Hints

- Use a `ref` with callback function: `<input type='text' ref={ ref => searchFieldRef = ref } />`

Or:
- Use a controlled component by storing the field value on the state using the `onChange` handler
- Use `onSubmit` handler to get form data (you'll need `event.preventDefault()` to [submit the form](https://facebook.github.io/react/docs/tutorial.html#submitting-the-form))

### Documentation

- [React Refs](https://facebook.github.io/react/docs/more-about-refs.html)
- [React Controlled Components](https://facebook.github.io/react/docs/forms.html#controlled-components)

### Solution:

```
git checkout solution-1
```


---

# Exercise 2

_Extract the search input to an own component_

### Hints

 - Hand down `onSubmit` callback function over props
 - Use a simple stateless functional component

### Docs

- https://facebook.github.io/react/docs/reusable-components.html#stateless-functions

### Solution:

```
git checkout solution-2
```

---

# Exercise 3

 _Query and display the next departures when a user clicks a station_
   1. Use `SbbDataService.getNextDeparturesFromStation(station)` to get the next departures
   2. Implement the click functionality on SbbStationList
   3. Implement a `SbbDepartures` component to display the departures

### Hints

 - The service returns a Promise. Have a look at the other service call in `MainView.js`
 - Use `.map()` to render a separate line per departure (see `SbbStationList.js`)
 - Add another component `Departure` to render a single departure item

### Documentation

 - API: http://transport.opendata.ch/docs.html#stationboard
 - JavaScript Promises: [MDN Documentation](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Promise)
 - [Array.prototype.map()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)

---

# What's next?
If you're interested in building "bigger" applications with react, you should consider the following links:
* Use a library to manage your state. We recommend Redux: https://github.com/rackt/redux
* Immutable.js to enforce unidirectional dataflow (your state should not be directly mutated by react components). Synergizes very well with Redux: https://facebook.github.io/immutable-js/
* React-Router for multiple routes: https://github.com/reactjs/react-router
* Have a look at the [ES6 class syntax](https://facebook.github.io/react/docs/reusable-components.html#es6-classes) for React components


# Resources

* https://facebook.github.io/react/
* https://facebook.github.io/react/docs/thinking-in-react.html
* https://github.com/rackt/redux (take a look at "examples")
* https://ponyfoo.com/articles/es6
* https://babeljs.io/
* https://github.com/erikras/react-redux-universal-hot-example (example app: redux with socket.io api)


---
# Redux

> Redux is a predictable state container for JavaScript apps.

 - The whole state of your app is stored in an object tree inside a single store.
 - The only way to change the state tree is to emit an action, an object describing what happened.
 - To specify how the actions transform the state tree, you write pure `reducers`.

https://github.com/reactjs/redux

![Redux](redux.png)


---

# Redux: Actions and Action Creators

 - Actions are payloads of information that send data from your application to your store.
   They are the only source of information for the store.
 - Actions are plain JavaScript objects. Actions must have a type property that indicates the type of action being performed.

```javascript
export const receiveStations = stations => ({
  type: 'RECIEVE_STATIONS',
  stations
});
```

---

# Redux: Reducer

 - Actions describe the fact that something happened, but don't specify how
   the application's state changes in response. This is the job of a reducer.
 - The reducer is a pure function that takes the previous state and an action,
   and returns the next state.

```javascript
const reducers = {
  RECIEVE_STATIONS: (state, action) => ({
    ...state,
    stations: action.stations
  })
}
```

 - Reducers are organized in separate files based on their respective state subtree
 - The reducer above is part of the `stationList` subtree

---

# React-Redux: connect Higher Order Component

> A Higher Order Component is just a React Component that wraps another one.

```javascript
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {fetchStations} from '../actions';

const MainView = ({stations, fetchStations}) => {
  return (
    <div/>
  );
}

export default connect(
  // mapStateToProps
  state => ({
    stations: state.stationList.stations
  }),
  // actionCreators
  dispatch => bindActionCreators({fetchStations}, dispatch)
)(MainView);
```

---

# Walkthrough

Our simple app with Redux.

- Store `Provider`
- Action Creators
- Reducers
- `connect` HOC

## Redux Branch
```
git checkout redux
```

---
# Redux: Exercise

_Add a favorite list feature to the app_
   1. The user should be able to add and remove stations from the favorite list
   2. _Extension_: Implement the timetable from Exercise 3 using Redux.

### Hints

 - Add a new Action `ADD_FAVORITE` and `REMOVE_FAVORITE`
 - Extend the application state with a list of favorites 

### Documentation

 - Redux: http://redux.js.org/
 - Actions: http://redux.js.org/docs/basics/Actions.html
 - Reducers: http://redux.js.org/docs/basics/Reducers.html
 - React-Redux: https://github.com/reactjs/react-redux

---
# Why React?

- Slim and simple
- Dataflow in large application is easy to understand (unidirectional dataflow)
- Reuseabilty out of the box
- Server-side rendering and performance

# Pain Points
- Expert know-how needed (JavaScript, state libaries)
- External dependencies
- Breaking changes
- Best practices had to evolve over time
